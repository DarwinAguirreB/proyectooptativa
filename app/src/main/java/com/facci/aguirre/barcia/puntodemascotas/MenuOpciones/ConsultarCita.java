package com.facci.aguirre.barcia.puntodemascotas.MenuOpciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.facci.aguirre.barcia.puntodemascotas.Menu;
import com.facci.aguirre.barcia.puntodemascotas.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class ConsultarCita extends AppCompatActivity {
    public static final String TAG = "TAG";
    TextView nMascota, dRaza, dEdad, dDueno, dTelefono, dPeso, dDireccion, muestraInfo,muestraInfo2,muestraInfo3,
            muestraPrecio, muestraPrecio2, muestraPrecio3, muestraDia, muestraHora, resultado;
    EditText Tipo, Raza, Edad, Sexo, Peso, Direccion, NombreMascota;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userId;
    Button btnSiguient, btnCalcular;
    Button resetPassLocal, changeProfileImage;
    FirebaseUser user;
    ImageView profileImage2;
    StorageReference storageReference;
    String userMascota;
    String userMas;
    String userAgendas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_consultar_cita);
        nMascota = findViewById(R.id.txtTraeMascota);
        dPeso = findViewById(R.id.txtDpeso);
        dRaza = findViewById(R.id.txtDraza);
        dEdad = findViewById(R.id.txtDedad);
        dDueno = findViewById(R.id.txtDdueno);
        dTelefono = findViewById(R.id.txtDnumerod);
        dDireccion = findViewById(R.id.txtDTdireccion);

        btnSiguient = findViewById(R.id.btnSiguiente);
        Tipo = findViewById(R.id.txtTipo);
        Raza = findViewById(R.id.txtRaza);
        Edad = findViewById(R.id.txtEdad);
        Sexo = findViewById(R.id.txtSexo);
        Peso = findViewById(R.id.txtPeso);
        Direccion = findViewById(R.id.txtDdireccion);
        NombreMascota = findViewById(R.id.textNmascota);

        muestraInfo=findViewById(R.id.txtServicio1);
        muestraInfo2=findViewById(R.id.txtServicio2);
        muestraInfo3=findViewById(R.id.txtServicio3);
        muestraPrecio=findViewById(R.id.txtValor1);
        muestraPrecio2=findViewById(R.id.txtValor2);
        muestraPrecio3=findViewById(R.id.txtValor3);
        muestraDia =findViewById(R.id.txtDiaServicio);
        muestraHora =findViewById(R.id.txtHoraServicio);
        resultado = findViewById(R.id.txtResultadoPagar);

        profileImage2 = findViewById(R.id.txtTraerImagen);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();



        userId = fAuth.getCurrentUser().getUid();
        userMas = fAuth.getCurrentUser().getUid();
        userMascota = fAuth.getCurrentUser().getUid();
        userAgendas= fAuth.getCurrentUser().getUid();
        user = fAuth.getCurrentUser();

        StorageReference profileRef = storageReference.child("users/"+fAuth.getCurrentUser().getUid()+"/profile.jpg");
        profileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(profileImage2);
            }
        });


        DocumentReference documentReference = fStore.collection("users").document(userId);
        DocumentReference documentReference2 = fStore.collection("mascotaYa").document(userMascota);
        DocumentReference documentReference3 = fStore.collection("agenda").document(userAgendas);
        documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {

                        if (documentSnapshot.exists()) {
                            dDueno.setText("Tu amigo es: "+ documentSnapshot.getString("fNombre"));
                            dTelefono.setText("Su celular es: "+ documentSnapshot.getString("fTelefono"));
                        } else {
                            Log.d("tag", "onEvent: Document do not exists");
                        }
                    }
                });
        documentReference2.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                    @Override
                    public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                        if (documentSnapshot.exists()) {
                            nMascota.setText("Hola "+documentSnapshot.getString("aMascota"));
                            dPeso.setText("Tu peso es: "+documentSnapshot.getString("aPeso"));
                            dRaza.setText("Eres de raza: "+documentSnapshot.getString("aRaza"));
                            dEdad.setText("Tu edad es: "+documentSnapshot.getString("aEdad"));
                            dDireccion.setText("Tu vives en: "+documentSnapshot.getString("aDireccion"));

                        } else {
                            Log.d("tag", "onEvent: Document do not exists");
                        }
                    }
                });

        documentReference3.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                if (documentSnapshot.exists()) {
                    muestraInfo.setText("Servicio 1 "+documentSnapshot.getString("cServicio"));
                    muestraInfo2.setText("Servicio 2: "+documentSnapshot.getString("cServicio2"));
                    muestraInfo3.setText("Servicio 3: "+documentSnapshot.getString("cServicio3"));
                    muestraPrecio.setText(" el valor es: "+documentSnapshot.getString("cPrecio"));
                    muestraPrecio2.setText(" el valor es: "+documentSnapshot.getString("cPrecio2"));
                    muestraPrecio3.setText(" el valor es: "+documentSnapshot.getString("cPrecio3"));
                    muestraHora.setText("Hora: "+documentSnapshot.getString("cHora"));
                    muestraDia.setText("Fecha: "+documentSnapshot.getString("cDia"));
                    resultado.setText("33 DOLARES");



                } else {
                    Log.d("tag", "onEvent: Document do not exists");
                }
            }
        });

            }



        }
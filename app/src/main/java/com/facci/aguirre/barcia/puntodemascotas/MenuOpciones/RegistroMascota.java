package com.facci.aguirre.barcia.puntodemascotas.MenuOpciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facci.aguirre.barcia.puntodemascotas.Menu;
import com.facci.aguirre.barcia.puntodemascotas.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class RegistroMascota extends AppCompatActivity {
    public static final String TAG = "TAG";
    TextView nMascota, dRaza, dEdad, dDueno, dTelefono, dPeso, dDireccion ;
    EditText Tipo, Raza, Edad, Sexo, Peso, Direccion, NombreMascota;
    FirebaseAuth fAuth;
    FirebaseFirestore fStore;
    String userId;
    Button btnSiguient, retroceder;
    Button resetPassLocal, changeProfileImage;
    FirebaseUser user;
    ImageView profileImage1;
    StorageReference storageReference;
    String userMascota;
    String userMas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_registro_mascota);

        nMascota = findViewById(R.id.txtTraeMascota);
        dPeso = findViewById(R.id.txtDpeso);
        dRaza = findViewById(R.id.txtDraza);
        dEdad = findViewById(R.id.txtDedad);
        dDueno = findViewById(R.id.txtDdueno);
        dTelefono = findViewById(R.id.txtDnumerod);
        dDireccion = findViewById(R.id.txtDTdireccion);

        btnSiguient = findViewById(R.id.btnSiguiente);

        Tipo = findViewById(R.id.txtTipo);
        Raza = findViewById(R.id.txtRaza);
        Edad = findViewById(R.id.txtEdad);
        Sexo = findViewById(R.id.txtSexo);
        Peso = findViewById(R.id.txtPeso);
        Direccion = findViewById(R.id.txtDdireccion);
        NombreMascota = findViewById(R.id.textNmascota);
        retroceder = findViewById(R.id.btnRetro);
        retroceder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RegistroMascota.this, Menu.class);
                startActivity(i);

            }
        });

        profileImage1 =findViewById(R.id.imagenPerfil);

        fAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();
        storageReference = FirebaseStorage.getInstance().getReference();


        userId = fAuth.getCurrentUser().getUid();
        userMas = fAuth.getCurrentUser().getUid();
        userMascota = fAuth.getCurrentUser().getUid();
        user = fAuth.getCurrentUser();

        StorageReference profileRef = storageReference.child("users/"+fAuth.getCurrentUser().getUid()+"/profile.jpg");
        profileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Picasso.get().load(uri).into(profileImage1);
            }
        });
        profileImage1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent openGalleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(openGalleryIntent,1000);
            }
        });

        btnSiguient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String mascota = NombreMascota.getText().toString().trim();
                final String tipo = Tipo.getText().toString();
                final String raza = Raza.getText().toString();
                final String edad = Edad.getText().toString();
                final String sexo = Sexo.getText().toString();
                final String peso = Peso.getText().toString();
                final String direccion = Direccion.getText().toString();


                userMas = fAuth.getCurrentUser().getUid();
                DocumentReference documentReference = fStore.collection("mascotaYa").document(userMas);
                Map<String, Object> user = new HashMap<>();
                user.put("aMascota", mascota);
                user.put("aTipo", tipo);
                user.put("aRaza", raza);
                user.put("aEdad", edad);
                user.put("aSexo", sexo);
                user.put("aPeso", peso);
                user.put("aDireccion", direccion);
                documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "onSuccess: user Profile is created for " + userMas);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: " + e.toString());
                    }
                });
                startActivity(new Intent(getApplicationContext(), Menu.class));
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @androidx.annotation.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1000){
            if(resultCode == Activity.RESULT_OK){
                Uri imageUri = data.getData();

                //profileImage.setImageURI(imageUri);

                uploadImageToFirebase(imageUri);


            }
        }

    }

    private void uploadImageToFirebase(Uri imageUri) {
        // uplaod image to firebase storage
        final StorageReference fileRef = storageReference.child("users/"+fAuth.getCurrentUser().getUid()+"/profile.jpg");
        fileRef.putFile(imageUri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                fileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                    @Override
                    public void onSuccess(Uri uri) {
                        Picasso.get().load(uri).into(profileImage1);
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(getApplicationContext(), "Failed.", Toast.LENGTH_SHORT).show();
            }
        });

    }
}
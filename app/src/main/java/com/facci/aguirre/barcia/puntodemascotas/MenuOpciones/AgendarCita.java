package com.facci.aguirre.barcia.puntodemascotas.MenuOpciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.facci.aguirre.barcia.puntodemascotas.Clases.Dias;
import com.facci.aguirre.barcia.puntodemascotas.Clases.Horas;
import com.facci.aguirre.barcia.puntodemascotas.Clases.Servicio;
import com.facci.aguirre.barcia.puntodemascotas.Menu;
import com.facci.aguirre.barcia.puntodemascotas.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

public class AgendarCita extends AppCompatActivity {

    public static final String TAG = "TAG";
    TextView muestraInfo,muestraInfo2,muestraInfo3, muestraPrecio, muestraPrecio2,
    muestraPrecio3, muestraDia, muestraHora;
    Button btnConf;
    Spinner spinnerServicios1, spinnerServicios2, spinnerServicios3, spinnerHora, spinnerDia;
    ;

    DatabaseReference mDataBase;
    FirebaseFirestore fStore;
    private FirebaseAuth firebaseAuth;
    String mServicoSeleccionado ="";
    String mServicoSeleccionado2 ="";
    String mServicoSeleccionado3 ="";
    String mHoraSeleccionada ="";
    String mDiaSeleccionada ="";
    String userAgenda;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agendar_cita);

        muestraInfo=findViewById(R.id.txtServicio1);
        muestraInfo2=findViewById(R.id.txtServicio2);
        muestraInfo3=findViewById(R.id.txtServicio3);
        muestraPrecio=findViewById(R.id.txtValor1);
        muestraPrecio2=findViewById(R.id.txtValor2);
        muestraPrecio3=findViewById(R.id.txtValor3);
        muestraDia =findViewById(R.id.txtDiaServicio);
        muestraHora =findViewById(R.id.txtHoraServicio);
        spinnerServicios1=findViewById(R.id.sPinnerServi1);
        spinnerServicios2=findViewById(R.id.sPinnerServi2);
        spinnerServicios3=findViewById(R.id.sPinnerServi3);


        spinnerDia = findViewById(R.id.sPinnerDia);
        spinnerHora = findViewById(R.id.sPinnerHora);


        btnConf=findViewById(R.id.btnConfAgenda);

        mDataBase= FirebaseDatabase.getInstance().getReference();
        loadService();

        firebaseAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        btnConf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String servicio =muestraInfo.getText().toString();
                final String servicio2 =muestraInfo2.getText().toString();
                final String servicio3 =muestraInfo3.getText().toString();
                final String precio  =muestraPrecio.getText().toString();
                final String precio2  =muestraPrecio2.getText().toString();
                final String precio3  =muestraPrecio3.getText().toString();
                final String horaM  =muestraHora.getText().toString();
                final String diaM =muestraDia.getText().toString();

                userAgenda = firebaseAuth.getCurrentUser().getUid();
                DocumentReference documentReference = fStore.collection("agenda").document(userAgenda);
                Map<String,Object> user = new HashMap<>();
                user.put("cServicio",servicio);
                user.put("cServicio2",servicio2);
                user.put("cServicio3",servicio3);
                user.put("cPrecio",precio);
                user.put("cPrecio2",precio2);
                user.put("cPrecio3",precio3);
                user.put("cHora",horaM);
                user.put("cDia",diaM);
                documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d(TAG, "onSuccess: user Profile is created for "+ userAgenda);
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d(TAG, "onFailure: " + e.toString());
                    }
                });
                startActivity(new Intent(getApplicationContext(), Menu.class));
            }
        });
    }

    public void loadService(){
        final List<Servicio> servicios = new ArrayList<>();
        mDataBase.child("Servicios").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot ds: snapshot.getChildren()){
                        String id =ds.getKey();
                        String nombre =ds.child("nombre").getValue().toString();
                        String precio =ds.child("precio").getValue().toString();
                        servicios.add(new Servicio(id, nombre, precio));
                    }
                    ArrayAdapter<Servicio> arrayAdapter = new ArrayAdapter<>(AgendarCita.this, android.R.layout.simple_dropdown_item_1line, servicios);
                    spinnerServicios1.setAdapter(arrayAdapter);
                    spinnerServicios2.setAdapter(arrayAdapter);
                    spinnerServicios3.setAdapter(arrayAdapter);

                    spinnerServicios1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            mServicoSeleccionado = adapterView.getItemAtPosition(i).toString();
                            muestraInfo.setText(mServicoSeleccionado);
                            String precio = servicios.get(i).getPrecio();
                            muestraPrecio.setText(precio);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    spinnerServicios2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            mServicoSeleccionado2 = adapterView.getItemAtPosition(i).toString();
                            muestraInfo2.setText(mServicoSeleccionado2);
                            String precio = servicios.get(i).getPrecio();
                            muestraPrecio2.setText(precio);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                    spinnerServicios3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            mServicoSeleccionado3 = adapterView.getItemAtPosition(i).toString();
                            muestraInfo3.setText( mServicoSeleccionado3);
                            String precio = servicios.get(i).getPrecio();
                            muestraPrecio3.setText(precio);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        //Spinner Hora

        final List<Horas> hora = new ArrayList<>();
        mDataBase.child("Horario").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot ds: snapshot.getChildren()){
                        String id =ds.getKey();
                        String nombre =ds.child("nombre").getValue().toString();
                        hora.add(new Horas(id, nombre));
                    }
                    ArrayAdapter<Horas> arrayAdapter = new ArrayAdapter<>(AgendarCita.this, android.R.layout.simple_dropdown_item_1line, hora);
                    spinnerHora.setAdapter(arrayAdapter);


                    spinnerHora.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            mHoraSeleccionada = adapterView.getItemAtPosition(i).toString();
                            muestraHora.setText(mHoraSeleccionada);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });


        //EspinnerDia

        final List<Dias> dias  = new ArrayList<>();
        mDataBase.child("Dia").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if (snapshot.exists()){
                    for (DataSnapshot ds: snapshot.getChildren()){
                        String id =ds.getKey();
                        String nombre =ds.child("nombre").getValue().toString();

                        dias.add(new Dias(id, nombre));
                    }
                    ArrayAdapter<Dias> arrayAdapter = new ArrayAdapter<>(AgendarCita.this, android.R.layout.simple_dropdown_item_1line, dias);
                    spinnerDia.setAdapter(arrayAdapter);


                    spinnerDia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            mDiaSeleccionada = adapterView.getItemAtPosition(i).toString();
                            muestraDia.setText(mDiaSeleccionada);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });





    }
}
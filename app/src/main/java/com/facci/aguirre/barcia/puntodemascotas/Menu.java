package com.facci.aguirre.barcia.puntodemascotas;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.GridLayout;
import android.widget.TextView;

import com.facci.aguirre.barcia.puntodemascotas.MenuOpciones.AgendarCita;
import com.facci.aguirre.barcia.puntodemascotas.MenuOpciones.Ajustes;
import com.facci.aguirre.barcia.puntodemascotas.MenuOpciones.ConsultarCita;
import com.facci.aguirre.barcia.puntodemascotas.MenuOpciones.Informacion;
import com.facci.aguirre.barcia.puntodemascotas.MenuOpciones.RegistroMascota;
import com.facci.aguirre.barcia.puntodemascotas.MenuOpciones.Servicios;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import javax.annotation.Nullable;



    public class Menu extends AppCompatActivity {

        GridLayout gridMenu;
        CardView cardMascota,cardAjustes,cardServicio,cardAgenda,cardConsulta,cardInfo;

        private static final int GALLERY_INTENT_CODE = 1023 ;
        TextView nombre,email;
        FirebaseAuth fAuth;
        FirebaseFirestore fStore;
        String userId;

        FirebaseUser user;
     //   StorageReference storageReference;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_menu);
            nombre = findViewById(R.id.txtNombre);
            email    = findViewById(R.id.profileEmail);

            cardMascota=findViewById(R.id.cardMascota);
            cardAjustes=findViewById(R.id.cardAjustes);
            cardServicio=findViewById(R.id.cardServicios);
            cardAgenda=findViewById(R.id.cardAgendar);
            cardConsulta=findViewById(R.id.cardConsulta);
            cardInfo=findViewById(R.id.cardInfo);


            fAuth = FirebaseAuth.getInstance();
            fStore = FirebaseFirestore.getInstance();

            userId = fAuth.getCurrentUser().getUid();
            user = fAuth.getCurrentUser();


            DocumentReference documentReference = fStore.collection("users").document(userId);
            documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                    if(documentSnapshot.exists()){
                        nombre.setText(documentSnapshot.getString("fNombre"));
                        email.setText(documentSnapshot.getString("email"));

                    }else {
                        Log.d("tag", "onEvent: Document do not exists");
                    }
                }
            });

            cardMascota.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Menu.this, RegistroMascota.class);
                    startActivity(i);
                }
            });
            cardConsulta.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Menu.this, ConsultarCita.class);
                    startActivity(i);
                }
            });
            cardServicio.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Menu.this, Servicios.class);
                    startActivity(i);
                }
            });
            cardAgenda.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Menu.this, AgendarCita.class);
                    startActivity(i);
                }
            });
            cardAjustes.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Menu.this, Ajustes.class);
                    startActivity(i);
                }
            });
            cardInfo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Menu.this, Informacion.class);
                    startActivity(i);
                }
            });


        }


    public void logout(View view) {
        FirebaseAuth.getInstance().signOut();//logout
        startActivity(new Intent(getApplicationContext(),Login.class));
        finish();
    }
}


package com.facci.aguirre.barcia.puntodemascotas;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.nfc.Tag;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class Registro extends AppCompatActivity {

    //defining view objects
    private EditText TextEmail;
    private EditText TextPassword;
    private EditText TextNombre;
    private EditText TextPhone;
    private Button btnRegistrar;
    public static final String TAG = "TAG";


    //Declaramos un objeto firebaseAuth
    private FirebaseAuth firebaseAuth;
    FirebaseFirestore fStore;
    String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registro);

        //inicializamos el objeto firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();
        fStore = FirebaseFirestore.getInstance();

        //Referenciamos los views
        TextEmail = (EditText) findViewById(R.id.editCorreRegis);
        TextPassword = (EditText) findViewById(R.id.editClave);
        TextNombre= (EditText) findViewById(R.id.editNombre);
        TextPhone= (EditText) findViewById(R.id.editPhone);

        btnRegistrar = (Button) findViewById(R.id.buttonRegis);


        //attaching listener to button

        if(firebaseAuth.getCurrentUser() != null){
            startActivity(new Intent(getApplicationContext(),Menu.class));
            finish();
        }


        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = TextEmail.getText().toString().trim();
                String password = TextPassword.getText().toString().trim();
                final String nombre = TextNombre.getText().toString();
                final String telefono =TextPhone.getText().toString();



                if (TextUtils.isEmpty(email)) {
                    TextEmail.setError("Se necesita un correo.");
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    TextPassword.setError("Se necesita una contraseña.");
                    return;
                }

                if (password.length() < 6) {
                    TextPassword.setError("Ingrese solo 6 carateres");
                    return;
                }

                firebaseAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){

                        Toast.makeText(Registro.this, "Usuario Creado.", Toast.LENGTH_SHORT).show();
                        userID = firebaseAuth.getCurrentUser().getUid();
                        DocumentReference documentReference = fStore.collection("users").document(userID);
                        Map<String,Object> user = new HashMap<>();
                        user.put("fNombre",nombre);
                        user.put("fTelefono",telefono);
                        user.put("email",email);
                        documentReference.set(user).addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d(TAG, "onSuccess: user Profile is created for "+ userID);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.d(TAG, "onFailure: " + e.toString());
                            }
                        });
                        startActivity(new Intent(getApplicationContext(),Menu.class));

                    }else {
                        Toast.makeText(Registro.this, "Error ! " + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }
            });
        }
    });
    }
}
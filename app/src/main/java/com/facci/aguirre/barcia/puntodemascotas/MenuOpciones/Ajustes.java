package com.facci.aguirre.barcia.puntodemascotas.MenuOpciones;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facci.aguirre.barcia.puntodemascotas.Login;
import com.facci.aguirre.barcia.puntodemascotas.MainActivity;
import com.facci.aguirre.barcia.puntodemascotas.Menu;
import com.facci.aguirre.barcia.puntodemascotas.R;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import javax.annotation.Nullable;


    public class Ajustes extends AppCompatActivity {
        private static final int GALLERY_INTENT_CODE = 1023 ;
        TextView fullName,email,mascota, celular;
        FirebaseAuth fAuth;
        FirebaseFirestore fStore;
        String userId;
        Button salir;
        Button resetPassLocal,changeProfileImage;
        FirebaseUser user;
        ImageView profileImage;
        StorageReference storageReference;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_ajustes);
            mascota =findViewById(R.id.txtMascoTrae);
            fullName = findViewById(R.id.nombre);
            email    = findViewById(R.id.profileEmail);
            celular =findViewById(R.id.txtPhoneEdit);
            resetPassLocal = findViewById(R.id.resetPasswordLocal);
            salir =findViewById(R.id.btnRegrMenu);

            salir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Ajustes.this, Menu.class);
                    startActivity(i);

                }
            });

            profileImage = findViewById(R.id.profileImage);
            changeProfileImage = findViewById(R.id.changeProfile);


            fAuth = FirebaseAuth.getInstance();
            fStore = FirebaseFirestore.getInstance();
            storageReference = FirebaseStorage.getInstance().getReference();

            StorageReference profileRef = storageReference.child("users/"+fAuth.getCurrentUser().getUid()+"/profile.jpg");
            profileRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                @Override
                public void onSuccess(Uri uri) {
                    Picasso.get().load(uri).into(profileImage);
                }
            });


            userId = fAuth.getCurrentUser().getUid();
            user = fAuth.getCurrentUser();





            DocumentReference documentReference = fStore.collection("users").document(userId);
            documentReference.addSnapshotListener(this, new EventListener<DocumentSnapshot>() {
                @Override
                public void onEvent(@Nullable DocumentSnapshot documentSnapshot, @Nullable FirebaseFirestoreException e) {
                    if(documentSnapshot.exists()){
                        celular.setText(documentSnapshot.getString("fTelefono"));
                        fullName.setText(documentSnapshot.getString("fNombre"));
                        email.setText(documentSnapshot.getString("email"));

                    }else {
                        Log.d("tag", "onEvent: Document do not exists");
                    }
                }
            });


            resetPassLocal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    final EditText resetPassword = new EditText(v.getContext());

                    final AlertDialog.Builder passwordResetDialog = new AlertDialog.Builder(v.getContext());
                    passwordResetDialog.setTitle("Reset Password ?");
                    passwordResetDialog.setMessage("Enter New Password > 6 Characters long.");
                    passwordResetDialog.setView(resetPassword);

                    passwordResetDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // extract the email and send reset link
                            String newPassword = resetPassword.getText().toString();
                            user.updatePassword(newPassword).addOnSuccessListener(new OnSuccessListener<Void>() {
                                @Override
                                public void onSuccess(Void aVoid) {
                                    Toast.makeText(Ajustes.this, "Password Reset Successfully.", Toast.LENGTH_SHORT).show();
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(Ajustes.this, "Password Reset Failed.", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    });

                    passwordResetDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // close
                        }
                    });

                    passwordResetDialog.create().show();

                }
            });

            changeProfileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // open gallery
                    Intent i = new Intent(v.getContext(),EditarPerfil.class);
                    i.putExtra("fNombre",fullName.getText().toString());
                    i.putExtra("email",email.getText().toString());
                    i.putExtra("fTelefono",celular.getText().toString());
                    startActivity(i);
//

                }
            });


        }

        public void logout(View view) {
            FirebaseAuth.getInstance().signOut();//logout
            startActivity(new Intent(getApplicationContext(), Login.class));
            finish();
        }


    }